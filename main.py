"""
This Python script processes tide sensor data from a CSV file and performs various analyses:

1. Reads data from the CSV file 'marees_v1.csv'.
2. Extracts sensor readings into the 'capteurs' list.
3. Defines a function 'affiche_capteurs' to display sensor data in a formatted manner.
4. Determines the position (x, y) on the beach where the tide reached its highest point.
5. Identifies points that are always submerged and those that are never submerged.
6. Calculates the percentage of time a specific point on the beach was underwater.
7. Determines the date with the highest and lowest tidal coefficients.

Functions:
- `affiche_capteurs(capteurs)`: Displays sensor data in a formatted table.
- `remove_values_from_list(the_list, val)`: Removes elements with a specified value from a list.
- `pourcentageImmerge(x, y, liste)`: Calculates the percentage of time a point on the beach is underwater.

Usage:
- Run the script to obtain results for the highest tide point, always submerged points, never submerged points,
  percentage of time a point is underwater, and dates with the highest and lowest tidal coefficients.
"""

# The rest of your code...


import csv

def affiche_capteurs(capteurs):
    """
    Displays the sensor data in a formatted table.

    Parameters:
    - capteurs (list): List containing sensor data.

    Returns:
    None
    """
    for row in capteurs:
        for item in row:
            print(f'{item:<15}', end='')  # Adjust the width as needed
        print()
    print("\n\n\n")


def remove_values_from_list(the_list, val):
    """
    Displays the sensor data in a formatted table.

    Parameters:
    - capteurs (list): List containing sensor data.

    Returns:
    None
    """
    return [value for value in the_list if value[0] != val]



def pourcentageImmerge(x,y,liste):
    """
    Calculates the percentage of time a specific point on the beach is underwater.

    Parameters:
    - x: X-coordinate of the point.
    - y: Y-coordinate of the point.
    - liste (list): List containing sensor data.

    Returns:
    float: Percentage of time the specified point is underwater.
    """
    compteurTotal = 0
    compteurImmerge = 0

    for unCapteur in liste:
        if unCapteur[1] == x and unCapteur[2] == y:
            print("ok")
            compteurTotal +=1
            if unCapteur[4] == "1":
                compteurImmerge+=1

    return (compteurImmerge/compteurTotal)*100









csv_file_path = 'marees_v1.csv'

capteurs = []


with open(csv_file_path, 'r', encoding='utf-8') as file:
    csv_reader = csv.reader(file)

    for row in csv_reader:
        capteurs.append(row)

#print(capteurs)
capteurs.remove(capteurs[0])
#print(capteurs + "\n\n\n")


#Donnez la position (x,y) sur la plage, où la marée a atteint le plus haut point
capteurMarreMax = capteurs[0]
for unCapteur in capteurs:
    if unCapteur[4] >= capteurMarreMax[4] and unCapteur[2]>= capteurMarreMax[2]:
        capteurMarreMax = unCapteur

#print(capteurMarreMax)


#Donnez l'ensemble des points qui se retrouvent toujours immergés

toujoursImmerges = capteurs
for unCapteur in capteurs:
    if unCapteur[4] == "0":
        toujoursImmerges = remove_values_from_list(toujoursImmerges, str(unCapteur[0]))

#print("\n")
#print(toujoursImmerges)
#affiche_capteurs(toujoursImmerges)


#Donnez l'ensemble des points qui ne se retrouvent jamais immergés
print("\n")
jamaisImmerges = capteurs
for unCapteur in capteurs:
    if unCapteur[4] == "1":
        jamaisImmerges = remove_values_from_list(jamaisImmerges, str(unCapteur[0]))

#print(jamaisImmerges)
#affiche_capteurs(jamaisImmerges)



#Pour un point quelconque sur la plage, et pour une période quelconque, indiquez le pourcentage de temps pendant lequel ce point s'est retrouvé sous l'eau.
x = "0"
y = "0"
#print("La balise située en (" +x+","+y+") est immérgée "+str(pourcentageImmerge(x,y, capteurs))+ "% du temps")



#Donnez la date du jour où il y a eu le plus gros coefficient de marée
totals = {}
actifs = {}
for unCapteur in capteurs:
    date = unCapteur[3]
    if date not in totals:
        totals[date] = 0
        actifs[date] = 0

    totals[date] += 1
    actifs[date] += int(unCapteur[4])

coefficients = {date: actifs[date] / totals[date] for date in totals}
print(coefficients, "\n")

date_max_coef = max(coefficients, key=coefficients.get)
print("La date du jour avec le plus gros coefficient de marée est :", date_max_coef)


#Donnez la date du jour où il y a eu le plus faible coefficient de marée
date_min_coef = min(coefficients, key=coefficients.get)
print("La date du jour avec le plus petit coefficient de marée est :", date_min_coef)
